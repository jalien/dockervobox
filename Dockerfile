FROM gitlab-registry.cern.ch/linuxsupport/alma9-base

# Add safeguards, repos and packages
# hadolint ignore=DL3033
RUN sed -i '$ d' /etc/dnf/dnf.conf

RUN curl https://repository.egi.eu/sw/production/cas/1/current/repo-files/egi-trustanchors.repo -o /etc/yum.repos.d/egi-trustanchors.repo && \
    dnf -y install http://linuxsoft.cern.ch/wlcg/el9/x86_64/wlcg-repo-1.0.0-1.el9.noarch.rpm && \
    dnf -y install https://dl.fedoraproject.org/pub/epel/epel-release-latest-9.noarch.rpm && \
    dnf -y install dnf-plugins-core && \
    dnf config-manager --set-enabled crb && \
    dnf -y install ui --skip-broken && \
    dnf -y install http://linuxsoft.cern.ch/wlcg/el9/x86_64/HEP_OSlibs-9.0.0-3.el9.x86_64.rpm && \
    dnf -y install wlcg-vobox && \
    dnf -y install gsi-openssh-server gsi-openssh-clients dnsmasq rsyslog autofs HEP_OSlibs net-tools apr-util lsof && \
    dnf -y install openssh openssh-clients openssh-server passwd && \
    dnf -y install python-setuptools inotify-tools python-pip && \
    dnf -y install https://research.cs.wisc.edu/htcondor/repo/10.x/htcondor-release-current.el9.noarch.rpm && \
    dnf -y install condor && \
    dnf -y install cronie && \
    dnf -y install iptables && \
    dnf -y install globus-proxy-utils && \
    dnf -y install myproxy ca-policy-egi-core ca-policy-lcg fetch-crl && \
    dnf -y install https://linuxsoft.cern.ch/wlcg/el9/x86_64/wlcg-voms-alice-1.0.0-1.el9.noarch.rpm && \
    dnf -y install man man-pages man-db && \
    dnf clean all && \
    pip install supervisor   

# Setup ssh access and add user(s)
# hadolint ignore=DL4006
RUN /usr/bin/ssh-keygen -A && \ 
    echo "PasswordAuthentication yes" >> /etc/ssh/sshd_config && \ 
    openssl rand -base64 12 | passwd --stdin root && \
    useradd -p "$(openssl rand -base64 12)" alicesgm
RUN /usr/bin/gsissh-keygen -q -t ed25519 -f /etc/gsissh/ssh_host_ed25519_key -C '' -N '' >&/dev/null && \
    chmod 600 /etc/gsissh/ssh_host_ed25519_key && \
    chmod 644 /etc/gsissh/ssh_host_ed25519_key.pub
    
###CONFIG###
RUN echo "alias ll='ls -la'" >> /root/.bash_profile
RUN echo -e "export LC_ALL=C\nexport LANG=C\nexport LANGUAGE=C" >> /etc/bashrc

COPY ./configs/vobox/etc/vobox-proxy.conf /var/lib/vobox/alice/etc/
RUN mkdir /var/lib/vobox/alice/stop && \
    chown -R alicesgm.alicesgm /var/lib/vobox/alice/etc && \
    chown -R alicesgm.alicesgm /var/lib/vobox/alice/stop 

#supervisord
COPY ./configs/supervisord/supervisord.conf /etc/

#alicesgm user
COPY ./configs/alicesgm/ /home/alicesgm
# hadolint ignore=SC2039
RUN mkdir -p /home/alicesgm/bin && ln -s /cvmfs/alice.cern.ch/bin/alienv /home/alicesgm/bin && \
    touch /home/alicesgm/enable-sandbox && mkdir /home/alicesgm/htcondor && mkdir /home/alicesgm/.globus && \
    mkdir -p /home/alicesgm/ALICE/alien-logs && ln -s /dev/null /home/alicesgm/ALICE/alien-logs/access_log && \
    chown -R alicesgm.alicesgm ~alicesgm

#iptables
COPY ./configs/iptables/iptables /etc/sysconfig/
COPY ./configs/iptables/ip6tables /etc/sysconfig/

#voms
COPY ./configs/voms/lcg-voms2.cern.ch.lsc /etc/grid-security/vomsdir/alice/
COPY ./configs/voms/voms2.cern.ch.lsc /etc/grid-security/vomsdir/alice/
COPY ./configs/voms/alice-lcg-voms2.cern.ch /etc/vomses/
COPY ./configs/voms/alice-voms2.cern.ch /etc/vomses/

#gsisshd
RUN rm /etc/gsissh/sshd_config
RUN rm /etc/gsissh/sshd_config.d/50-redhat.conf
COPY ./configs/gsisshd/sshd_config /etc/gsissh/
RUN update-crypto-policies --set DEFAULT:SHA1 || exit

#rsyslog
RUN rm /etc/rsyslog.conf
COPY ./configs/rsyslog/rsyslog.conf /etc/rsyslog.conf

#myproxy
RUN echo "export MYPROXY_SERVER=myproxy.cern.ch" >> /etc/profile.d/grid-env.sh

#cron
COPY ./configs/cron/alice-box-proxyrenewal /etc/cron.d/
COPY ./configs/cron/edg-mkgridmap /etc/cron.d/
COPY ./configs/cron/edg-mkgridmap.conf /etc/
COPY ./configs/cron/grid-mapfile-local /etc/
COPY ./configs/cron/alicesgm /var/spool/cron/
RUN chown -R alicesgm.alicesgm /var/spool/cron/alicesgm && chmod 600 /var/spool/cron/alicesgm && \
    chmod 644 /etc/cron.d/[ae]*

#condor
COPY ./configs/condor/00-minicondor.vobox /etc/condor/config.d/
COPY ./configs/condor/02_container_extra.config /etc/condor/config.d/
COPY ./configs/condor/99-alice-vobox.conf /etc/condor/config.d/
COPY ./configs/condor/ce-usage.sh /home/alicesgm/
RUN chown -R alicesgm.alicesgm /home/alicesgm/ce-usage.sh && \
    chmod a+x /home/alicesgm/ce-usage.sh

#more manpages
COPY ./extras/manpage_bundle.tar.gz /tmp
RUN mkdir -p /usr/share/man/overrides/ && tar -xf /tmp/manpage_bundle.tar.gz -C /usr/share/man/overrides/ && mandb && rm /tmp/manpage_bundle.tar.gz

#use a "last" without nologout bug
RUN rm /usr/bin/last
COPY ./extras/last /usr/bin/

#JAlien-VOBox (CVMFS shortcut function)
RUN echo 'jalien-vobox () { /cvmfs/alice.cern.ch/scripts/vobox/jalien-vobox.sh "$@"; }' >> /etc/bashrc

###INIT###

#Add init and service scripts
COPY ./init.sh /init.sh
COPY ./services/* /services/
RUN mkdir -p /etc/init.d/ && \
    mkdir -p /etc/rc.d/init.d/ && \
    chmod u+x /init.sh && \
    chmod -R u+x /services && \
    ln -s /services/alice-box-proxyrenewal /etc/init.d/
#    mv /etc/rc.d/init.d/functions /etc/rc.d/init.d/functions2 && \
#    ln -s /services/functions /etc/rc.d/init.d/

#Add function to give users/scripts systemd-like call for supervisor
RUN echo 'systemctl () { supervisorctl "$@"; }' >> /etc/bashrc

#Run init script upon container start
CMD [ "/init.sh" ]
