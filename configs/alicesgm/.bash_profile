# .bash_profile

# Get the aliases and functions
if [ -f ~/.bashrc ]; then
        . ~/.bashrc
fi

# User specific environment and startup programs

PATH=$PATH:$HOME/.local/bin:$HOME/bin

export PATH

proxy()
{
    local tmp=`ls -t /var/lib/vobox/alice/proxy_repository/*lcgadmin | sed q`

    if [ ${tmp:+x} ]
    then
        export X509_USER_PROXY=$tmp
        echo Defining the right proxy...
    else
        echo Could not define the right proxy...
    fi
}

tty -s && proxy

alias ll='ls -la'
