#!/bin/bash
# crontab entry:
# 58 3 * * * /bin/sh $HOME/cron/cleanup-tmp.sh > /dev/null 2>&1 < /dev/null

LOG=/tmp/cleanup-tmp.`date +%y%m%d`.log

exec >> $LOG 2>&1 < /dev/null

echo "=== START `date` PID $$"

cd /tmp

user=$(id -nu)

[ -e $user ] || (umask 077; mkdir $user)

find . $user -maxdepth 1 -user $user -type f \( \
        -name cleanup-ALICE-tmp.\*.log -o \
        -name cleanup-held-jobs-\*.log -o \
        -name cleanup-tmp.\*.log -o \
        -name confApMon.\* -o \
        -name ipcrm-\*.txt -o \
        -name jalien-check.\*.log -o \
        -name valuesApMon.\* -o \
        -name vobox-check-\*.log -o \
        -name x509up_\* -o \
        \( -size +0 -mtime +30 \) \
    \) -mtime +7 -ls -exec /bin/rm {} \;

echo "=== READY `date` PID $$"
