#!/bin/sh

cd ~/htcondor || exit

GZ_SIZE=10k
GZ_MINS=60
GZ_DAYS=2
#RM_DAYS=7
RM_DAYS=2

STAMP=.stamp

prefix=cleanup-
log=$prefix`date +%y%m%d`
exec >> $log 2>&1 < /dev/null

echo === START `date`

for d in `ls -d 20??-??-??`
do
    (
	echo === $d

	stamp=$d/$STAMP

	[ -e $stamp ] || touch $stamp || exit

	if find $stamp -mtime +$RM_DAYS | grep . > /dev/null
	then
	    echo removing...
	    /bin/rm -r $d < /dev/null
	    exit
	fi

	cd $d || exit

	find . ! -name .\* ! -name \*.gz \( -mtime +$GZ_DAYS -o \
            -size +$GZ_SIZE -mmin +$GZ_MINS \) -exec gzip -9v {} \;
    )
done

find $prefix* -mtime +$RM_DAYS -exec /bin/rm {} \;

echo === READY `date`

