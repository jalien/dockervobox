#!/bin/bash
# crontab entry:
# 56 3 * * * /bin/sh $HOME/cron/cleanup-ALICE-tmp.sh > /dev/null 2>&1 < /dev/null

cd ~/ALICE/tmp 2> /dev/null || exit 0

LOG=/tmp/cleanup-ALICE-tmp-$(date +'%y%m%d').log

exec >> $LOG 2>&1 < /dev/null

echo "=== START `date` PID $$"

user=$(id -nu)

find . -maxdepth 1 -user $user \( \
        -name agent.startup.'[1-9]*[0-9]' \
    \) -mtime +365 -ls -exec /bin/rm {} \;

(
    cd arc 2> /dev/null || exit 0

    find . -maxdepth 1 -user $user -name '20??-??-??' -mtime +30 \
        -ls -exec /bin/rm -r {} \;
)

echo "=== READY `date` PID $$"
