#!/bin/sh

c='jobstatus == 5 && CurrentTime - EnteredCurrentStatus > 10800'
date=`date +%y%m%d`
log=/tmp/cleanup-held-jobs-$date.log

(
    echo == START `date`
    condor_rm -constraint "$c"
    echo == READY `date`
) >> $log 2>&1 < /dev/null

