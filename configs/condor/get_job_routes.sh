#!/bin/bash
# print HTCondor job routes obtained from the ALICE LDAP server
#
# example settings in /etc/condor/config.d:
#
# JOB_ROUTER_ENTRIES_CMD = /var/lib/condor/get_job_routes.sh
# JOB_ROUTER_ENTRIES_REFRESH = 600
#
# version 1.3 (2017/04/04)
# author: Maarten Litmaath

usage()
{
    echo "Usage: $0 [-n] [ FQHN ]" >&2
    exit 1
}

LOG=/tmp/job-routes-$(date '+%y%m%d').log
LDAP_ADDR=alice-ldap.cern.ch:8389
h=$(hostname -f)

case $1 in
-n)
    LOG=
    shift
esac

case $1 in
-*)
    usage
    ;;
?*.?*.?*)
    h=$1
    ;;
?*)
    usage
esac

f="(&(objectClass=AlienCE)(host=$h))"

#
# wrapped example output lines returned by the ldapsearch:
#
# environment: ROUTES_LIST=\
# [ "condor ce503.cern.ch ce503.cern.ch:9619" ] \
# [ "condor ce504.cern.ch ce504.cern.ch:9619"; optional extra stuff ] \
# [ "condor ce505.cern.ch ce505.cern.ch:9619" ] \
# [ "condor ce506.cern.ch ce506.cern.ch:9619" ]
#
# or a simpler format (the port currently is needed for the SAM VO feed):
#
# environment: ROUTES_LIST=\
# [ ce503.cern.ch:9619 ] \
# [ ce504.cern.ch:9619; optional extra stuff ] \
# [ ce505.cern.ch:9619 ] \
# [ ce506.cern.ch:9619 ]
#
# the next line may even be absent:
#
# environment: USE_EXTERNAL_CLOUD=0
#

if [ "x$LOG" = x ]
then
    LOG=/dev/null
else
    echo == $(date) >> $LOG
    exec 2>> $LOG
fi

ldapsearch -LLL -x -h $LDAP_ADDR -b o=alice,dc=cern,dc=ch "$f" environment |
    perl -p00e 's/\r?\n //g' | perl -ne '
	if (s/^environment: ROUTES_LIST *= *//i) {
	    s/\[ *([^]" ]+)(:\d+) *([];])/[ "condor $1 $1$2" $3/g;
	    s/\[ *([^]" ]+) *([];])/[ "condor $1 $1:9619" $2/g;
	    s/\[ *[^"]*"/[ "/g;
	    s/\[ *("[^"]+")/[ GridResource = $1; eval_set_GridResource = $1/g;
	    $routes = $_;
	    next;
	}
	if (s/^environment: USE_EXTERNAL_CLOUD *= *//i) {
	    $extern = "; set_WantExternalCloud = True" if /1/;
	    next;
	}
	END {
	    $extern .= " ]";
	    $routes =~ s/;? *]/$extern/eg;
	    print $routes;
	}
    ' | tee -a $LOG


