#!/bin/sh

condor_q -const "JobStatus < 3" -af JobStatus GridResource |
    perl -ne '
	/(\d)\s.*\s(\S+)/ && $ce{$2}{$1}++;

	END {
	    $f = "%5s %5s %s\n";
	    printf $f, "Run", "Idle", "CE";

	    for (sort keys %ce) {
		$i_tot += $i = $ce{$_}{1} + 0;
		$r_tot += $r = $ce{$_}{2} + 0;
		printf $f, $r, $i, $_;
	    }

	    $s = "-----";
	    printf $f, $s, $s, "+";
	    printf $f, $r_tot + 0, $i_tot + 0, "at '"`date`"'";
	}
    '
