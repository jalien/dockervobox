#! /bin/bash

d=`date '+%y%m%d-%H%M%S'`
log=/tmp/init-$d.log

> $log && exec > $log 2>&1 < /dev/null

for i in '' 6
do
    f=/etc/sysconfig/ip"$i"tables
    [ -r $f ] && perl -ne '/^\s*-/ && system "ip'"$i"'tables $_"' < $f
done

iptables -A INPUT ! -s 127.0.0.1 -p tcp -m tcp --dport 9618 -j DROP
ip6tables -A INPUT ! -s localhost -p tcp -m tcp --dport 9618 -j DROP

supervisord -c /etc/supervisord.conf &
sleep 5
chmod 777 /var/run/supervisord.sock

rm /var/run/nologin &> /dev/null

#Keep the container running
while :; do sleep infinity; done

exit 0
